-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Oct 14, 2018 at 12:07 PM
-- Server version: 10.1.28-MariaDB
-- PHP Version: 7.1.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `registration_panelist`
--

-- --------------------------------------------------------

--
-- Table structure for table `accounts`
--

CREATE TABLE `accounts` (
  `id` int(11) NOT NULL,
  `username` varchar(50) NOT NULL,
  `password` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `accounts`
--

INSERT INTO `accounts` (`id`, `username`, `password`) VALUES
(1, 'rnd', '$2y$10$VijKYHDqZN3LWTfVA2uSS.BC9i3l2vzMoaVn3ZcBjFyDP5BWB0OAi');

-- --------------------------------------------------------

--
-- Table structure for table `events`
--

CREATE TABLE `events` (
  `id` int(11) NOT NULL,
  `event_name` varchar(255) NOT NULL,
  `template` varchar(30) NOT NULL,
  `date` datetime NOT NULL,
  `venue` varchar(30) NOT NULL,
  `cert_type` varchar(15) NOT NULL,
  `account_id` int(11) NOT NULL,
  `target_table` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `events`
--

INSERT INTO `events` (`id`, `event_name`, `template`, `date`, `venue`, `cert_type`, `account_id`, `target_table`) VALUES
(1, 'Thesis Defense', 'codite', '2018-09-22 07:00:00', 'CCSS Lobby', 'landscape', 1, 'panelists'),
(2, 'Asking the better questions', 'askingthebetter', '2018-09-26 12:00:00', 'MPH', 'landscape', 1, 'participants'),
(3, 'Do less. be more: living a purposeful college life', 'doless', '2018-09-27 12:00:00', 'MPH', 'landscape', 1, 'participants'),
(4, 'codite', 'codite', '2018-10-16 04:30:00', 'ue cal', 'landscape', 1, 'participantsss');

-- --------------------------------------------------------

--
-- Table structure for table `font`
--

CREATE TABLE `font` (
  `id` int(11) NOT NULL,
  `event_id` int(11) NOT NULL,
  `font_name` varchar(30) NOT NULL,
  `font_size` int(11) NOT NULL,
  `pos_x` int(11) NOT NULL,
  `pos_y` int(11) NOT NULL,
  `color_r` int(3) NOT NULL,
  `color_g` int(3) NOT NULL,
  `color_b` int(3) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `font`
--

INSERT INTO `font` (`id`, `event_id`, `font_name`, `font_size`, `pos_x`, `pos_y`, `color_r`, `color_g`, `color_b`) VALUES
(1, 1, 'LemonMilk', 40, 20, 97, 255, 255, 255),
(2, 2, 'Cambria', 40, 20, 105, 0, 0, 0),
(3, 3, 'Cambria', 40, 20, 110, 0, 0, 0),
(4, 4, 'LemonMilk', 40, 30, 97, 255, 255, 255);

-- --------------------------------------------------------

--
-- Table structure for table `panelists`
--

CREATE TABLE `panelists` (
  `id` int(11) NOT NULL,
  `firstname` varchar(30) NOT NULL,
  `lastname` varchar(30) NOT NULL,
  `mi` varchar(4) NOT NULL,
  `email` varchar(64) NOT NULL,
  `company` varchar(255) NOT NULL,
  `contact` varchar(11) NOT NULL,
  `gender` varchar(1) NOT NULL,
  `event_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `panelists`
--

INSERT INTO `panelists` (`id`, `firstname`, `lastname`, `mi`, `email`, `company`, `contact`, `gender`, `event_id`) VALUES
(3, 'Geecee Maybelline', 'Manabat', 'A', 'geecee.manabat@ue.edu.ph', 'University of the East', '09166278942', 'F', 1),
(4, 'Roy', 'Callope', 'B', 'rbcallope@gmail.com', 'University of the East', '09209463906', 'M', 1),
(5, 'Janusvielle', 'Zate', 'A', 'janus.aragones@gmail.com', 'University of the East', '09178666472', 'F', 1),
(6, 'Kristian', 'Gonzales', 'S', 'kristian.s.gonzales@accenture.com', 'Accenture', '09985946215', 'M', 1),
(7, 'Mary Grace', 'Magcale - Moreno', 'M', 'marygrace.moreno@arrow.com', 'Outsourced - Arrow ECS ANZ', '09088209070', 'F', 1),
(8, 'Ariel', 'Aviles', 'D', 'CO113@YAHOO.COM', 'University of the East ', '09154668888', 'M', 1),
(9, 'Paul Gilbert', 'Maglaya', 'V', 'GILNICX_2000@YAHOO.COM', 'University of the East', '09271509953', 'M', 1),
(10, 'Mary Grace', 'Ventura', 'G', 'marygrace.ventura@ue.edu.ph', 'University of the East', '0905471772', 'F', 1),
(11, 'Charles Candido', 'Cammayo', 'M', 'CHARLES.CAMMAYO@ALORICA.COM', 'ALORICA PHILIPPINES INC.', '09327957354', 'M', 1),
(12, 'Pauline', 'Milan', 'A', 'pauline.milan@sageninja.com', 'Sage Ninja Creatives Co', '09177062884', 'F', 1),
(13, 'Mark Kristoffer', 'Pradillada', 'A', 'mpradillada@gmail.com', 'Accenture Inc.', '09174646255', 'M', 1),
(14, 'Kimberly', 'Trinidad', 'M', 'kimberlymtrinidad@yahoo.com', 'TERADATA PHILIPPINES INC.', '09778104598', 'F', 1),
(15, 'Krystel Gayle', 'Manzanilla', 'M', 'gayle.manzanilla@gmail.com', 'Aurecon', '09777233130', 'F', 1),
(16, 'Everard', 'Onggon', 'C', 'everard@thenextjump.com', 'The Next Jump', '09062831310', 'M', 1),
(17, 'Sherwine', 'Cordero', 'A', 'whine23@gmail.com', 'Trend Micro Inc.', '09277991153', 'F', 1),
(19, 'Ashraf', 'Encila', 'N', 'ashEncila@gmail.com', 'Arch Global Services Inc.', '09176345665', 'M', 1),
(20, 'Kimberly Ann', 'Raymundo', 'F', 'raymundo.kiann@gmail.com', 'Nowcom Global Services, LLC', '09173268227', 'F', 1),
(21, 'Ma. Teresa', 'Borebor', 'F', 'teresa.borebor@ue.edu.ph', 'University of the East', '09178585011', 'F', 1),
(22, 'Lea', 'Pasion', 'C', 'PASION.LEA@BDO.COM.PH', 'BANCO DE ORO UNIBANK INC.', '09253031973', 'F', 1),
(23, 'Jan Rino', 'Sabile', 'T', 'janrinosabile@gmail.com', 'SMS GLOBAL TECHNOLOGIES INC.', '09151824129', 'M', 1),
(25, 'Carl Louise', 'McDonough', 'B', 'carllouisemcdonough08@gmail.com', 'Schneider Electric Asia Ltd.', '09175958148', 'M', 1),
(26, 'Roselyn', 'Alcantara', 'I', 'roselynalcantara@gmail.com', 'E-Tech Business Solutions', '09266807692', 'F', 1),
(27, 'Lionell', 'Libarios', 'H', 'lionelllibarios12@yahoo.com', 'RisingTide Mobile Inc.', '09175027146', 'M', 1),
(28, 'Eugene', 'Constantino', 'E', 'dh.eugeneconstantino@gmail.com', 'Digital Hog Online Marketing Services Inc.', '09163009286', 'M', 1),
(29, 'Roselle', 'Basa', 'S', 'roselle.basa@ue.edu.ph', 'University of the East', '09177949329', 'F', 1),
(31, 'Marivic', 'Gatus', 'M', 'marivicgatus@ue.edu.ph', 'University of the East', '09353006470', 'F', 1),
(32, 'Katrine Mae', 'Alba', 'C', 'katrinemaealba@gmail.com', 'FedEx', '09086092094', 'F', 1),
(33, 'Theresse', 'Garcera', 'G', 'garceratheresse@yahoo.com', 'Alorica', '09055655684', 'F', 1),
(34, 'Bryan', 'Oab', 'D', 'bryan.oab25@gmail.com', 'Accenture - Avanade', '63916336867', 'M', 1),
(35, 'Julie Anne', 'Polican', 'S', 'julieannepolican@gmail.com', 'CheQ Systems', '09566092203', 'F', 1),
(36, 'Mark Anthony', 'Uy', 'D', 'markanthony.uy@ue.edu.ph', 'University of the East - Manila', '09184314710', 'M', 1),
(37, 'Anna Jane', 'Matillano', 'O', 'aomatillano@gmail.com', 'Salarium', '09283912143', 'F', 1),
(38, 'Marita', 'Tolentino', 'N', 'marita.tolentino@ue.edu.ph', 'University of the East', '09167298937', 'F', 1),
(39, 'Ma. Ymelda', 'Batalla', 'C', 'maymelda.batalla@ue.edu.ph', 'University of the East', '09279725095', 'F', 1),
(40, 'Melie Jim', 'Sarmiento', 'F', 'mjfsarmiento@ahoo.com', 'University of the East', '09159128040', 'F', 1),
(41, 'Mary Joyce', 'Rillon', 'G', 'joyce_rillon@yahoo.com', 'Oracle Philippines', '63917113752', 'F', 1),
(42, 'Dionisio', 'Miguel Jr.', 'L', 'donlmiguel@gmail.com', 'BDO Unibank, Inc.', '09167671395', 'M', 1),
(43, 'Arne', 'Bana', 'R', 'arne.bana@ue.edu.ph', 'University of the East - Manila', '09178978959', 'M', 1),
(44, 'Ralph Alvin', 'Almeda', 'W', 'rwalmeda15@gmail.com', 'BPI', '09052700517', 'M', 1),
(45, 'Miguel Angelo', 'Tolentino', 'A', 'miguelangelo.tolentino@ue.edu.ph', 'University of the East', '09175427318', 'M', 1),
(46, 'Reynaldo', 'Zausa', 'B', 'reyzausa@gmail.com', 'Cheq Systems Inc. ', '09178826204', 'M', 1),
(47, 'Reynaldo Gerald', 'Endaya II', 'T', 'reynaldogeraldendaya@yahoo.com', 'Shang Software Solutions Inc.', '09278610291', 'M', 1),
(48, 'Henry Dyke', 'Balmeo', 'A', 'henrydyke@yahoo.com', 'University of the East - IT Department', '09178405165', 'M', 1),
(49, 'Marc Rodin', 'Ligas', 'C', 'marcrodin.ligas@ue.edu.ph', 'University of the East', '09055926873', 'M', 1),
(50, 'Alvin', 'Gutierrez', 'T', 'alvingjohndrich@gmail.com', 'Bank Of Tokyo MUFG', '9285034660', 'M', 1),
(51, 'May Greeze', 'de Zosa', 'M', 'maygreeze.dezosa@ue.edu.ph', 'University of the East', '09086111856', 'F', 1),
(52, 'Christalyn', 'Maramag', 'B', 'cristal.maramag@gmail.com', 'AXA Philippines', '09278838818', 'F', 1),
(53, 'Rudyard', 'Pesimo', 'C', 'rcpesimo@gmail.com', 'University of the East - Manila', '09771005101', 'M', 1),
(55, 'Andrei Ramases', 'Quitalig', 'B', 'andrei_quitalig@yahoo.com', 'SAP Philippines', '09178247742', 'M', 1),
(56, 'Isaac', 'Mongcal', 'Z', 'isaac.mongcal@gmail.com', 'University of the East', '09355003034', 'M', 1),
(57, 'Raymond', 'Valdez', 'E', 'esper21gi@gmail.com', 'Moon Eaters Studio', '09162022178', 'M', 1),
(58, 'Felicito', 'Nuarin', 'S', 'chito.nuarin@ue.edu.ph', 'University of the East', '09199927322', 'M', 1),
(59, 'Jessa Marie', 'Bernardo', 'E', 'jessamebernardo@gmail.com', 'Willis Towers Watson', '09162090981', 'F', 1),
(60, 'Reynalyn', 'Elon', 'T', 'reynalyn.elon@ue.edu.ph', 'University of the East', '09958964923', 'F', 1),
(62, 'Julie Vernes', 'Rodriguez', 'C', 'julievernes.rodriguez@gmail.com', 'Accenture, Inc.', '09175734141', 'F', 1),
(63, 'Ferdinand', 'Hipolito', 'A', 'ferdie.hipolito@toyotanorthedsa.com', 'Toyota North EDSA', '09951729164', 'M', 1),
(64, 'Mark Anthony', 'Galang', 'S', 'makgalang@gmail.com', 'Capgemini Philippines', '09271529042', 'M', 1),
(65, 'Brenda', 'Basas', 'F', 'basasbrenda@gmail.com', 'GVC', '09773289800', 'F', 1),
(66, 'Cynirene', 'Carillo', 'C', 'cynairenecc@yahoo.com', 'Sykes Asia Inc.', '09989655498', 'F', 1),
(67, 'Elijah', 'Kayguan', 'V', 'elijah.kayguan@yahoo.com', 'Shang Software Solutions Inc.', '09154716520', 'M', 1),
(68, 'Bryan Carlos', 'Andico', 'A', 'andicobryan@gmail.com', 'Unilab Inc.', '09772313977', 'M', 1),
(69, 'Bryand Gabriel', 'Rasco', 'M', 'bryce2006@gmail.com', 'UE', '09052596245', 'M', 1),
(70, 'Benjamin', 'Maure', 'E', 'benjiemaure@gmail.com', 'Xentrix Toons Inc.', '09178755778', 'M', 1),
(71, 'Ronald', 'Orcino', 'R', 'mhacdho@yahoo.com', 'Emerson Electric ASIA ROHQ', '09178415339', 'M', 1),
(72, 'Maria Elena', 'Valenzuela', 'P', 'elaine5ph@yahoo.com', 'National Irrigation Administration', '09225309180', 'F', 1),
(73, 'Pierre', 'Carag', 'C', 'apoi_carag@yahoo.com', 'Bank of the Philippine Islands', '09209079091', 'M', 1),
(74, 'Nathex-Kasem', 'Iwuanyanwu Jr', 'F', 'iwuanyanwu_nathex@yahoo.com', 'Oracle Philippines', '09175985053', 'M', 1),
(75, 'Edmon', 'Torres', 'L', 'edmon.torres@ue.edu.ph', 'University of the East', '09957010715', 'M', 1),
(76, 'Johnny', 'Hermoso', 'B', 'jhoy_28@yahoo.com', 'University of the East', '09391086830', 'M', 1),
(77, 'Jerry', 'Punzalan', 'f', 'jerrypunzalan24@gmail.com', 'wqeqwe', '9156155821', 'M', 1),
(78, 'Jerryyyy', 'Punzalannnn', 'f', 'jerrypunzalan24@gmail.com', 'qweqwewqe', '09156155821', 'M', 1),
(79, 'jerryyy', 'punzalan', 'f', 'achtzeeehnypunzalan24@gmail.com', '23231', '123123', 'M', 1);

-- --------------------------------------------------------

--
-- Table structure for table `participants`
--

CREATE TABLE `participants` (
  `id` int(11) NOT NULL,
  `event_id` int(11) NOT NULL,
  `firstname` varchar(50) NOT NULL,
  `lastname` varchar(50) NOT NULL,
  `mi` varchar(4) NOT NULL,
  `gender` varchar(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `participants`
--

INSERT INTO `participants` (`id`, `event_id`, `firstname`, `lastname`, `mi`, `gender`) VALUES
(4, 2, 'Jerry', 'Punzalan', 'f', 'M'),
(5, 3, 'Jerry', 'Punzalan', 'f', 'M');

-- --------------------------------------------------------

--
-- Table structure for table `participantsss`
--

CREATE TABLE `participantsss` (
  `id` int(10) UNSIGNED NOT NULL,
  `event_id` int(11) NOT NULL,
  `firstname` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `lastname` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `mi` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `gender` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `school` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `participantsss`
--

INSERT INTO `participantsss` (`id`, `event_id`, `firstname`, `lastname`, `mi`, `gender`, `school`) VALUES
(1, 4, 'jerryyyyyyyreee', 'punzalan', 'f', 'M', 'cogeo'),
(2, 4, 'jerryyyee', 'punzalan', 'f', 'M', 'qwewqe'),
(3, 4, 'jerryyy', 'punzalan', 'f', 'M', 'qwewqe'),
(4, 4, 'jerry', 'punzalan', 'f', 'M', 'university of the east');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `accounts`
--
ALTER TABLE `accounts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `events`
--
ALTER TABLE `events`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `font`
--
ALTER TABLE `font`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `panelists`
--
ALTER TABLE `panelists`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `participants`
--
ALTER TABLE `participants`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `participantsss`
--
ALTER TABLE `participantsss`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `accounts`
--
ALTER TABLE `accounts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `events`
--
ALTER TABLE `events`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `font`
--
ALTER TABLE `font`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `panelists`
--
ALTER TABLE `panelists`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=80;

--
-- AUTO_INCREMENT for table `participants`
--
ALTER TABLE `participants`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `participantsss`
--
ALTER TABLE `participantsss`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
