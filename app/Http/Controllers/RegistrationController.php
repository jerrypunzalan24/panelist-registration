<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;

class RegistrationController extends Controller
{
  public function index(Request $request){
    $table = $request->session()->get("table") ?? "panelists";
    $columns = \DB::select("DESCRIBE {$table}");
    return view('registration.index',[
      'columns' => $columns,
      'link'=> route("home"),
      'name'=>"<i class ='fa fa-arrow-left'></i> Go Back",
      "style" =>"cursor:pointer",
      "login"=> true ]);
  }
  public function participants(Request $request){
    $table = $request->session()->get("table") ?? "panelists";
    $columns = \DB::select("DESCRIBE {$table}");
    $participants = \DB::table($table)->where("event_id",$request->session()->get("event_id"))->orderBy("id", "desc")->get();
    return view('participants.index', [
      'columns' => $columns,
      'participants' => $participants,
      'link'=>'./',
      'name'=>"<i class ='fa fa-arrow-left'></i> Go Back",
      "style" =>"cursor:pointer",
      "login" => true]);
  }
  public function addparticipant(Request $request){
    $table = $request->session()->get("table") ?? "panelists";
    $event_id = $request->session()->get("event_id") ?? 1;
    $columns = json_decode(json_encode(\DB::select("DESCRIBE {$table}")),true);
    $columnstr = '';
    $valuestr='';
    foreach($columns as $row){
      if($row['Field'] != 'id' && $row['Field'] != 'event_id'){
        $columnstr .= $row['Field'] . ',';
        if(strpos($row['Type'], 'int') == 0){
          $valuestr .= "'" .$request->input($row['Field'])."',";
        }
        else{
          $valuestr .= $request->input($row['Field']).",";
        }
      }
    }
    $columnstr = substr($columnstr, 0 , strlen($columnstr)-1);
    $valuestr = substr($valuestr, 0,strlen($valuestr)-1);
    \DB::select("INSERT INTO {$table}({$columnstr},event_id) VALUES($valuestr,$event_id)");
  }

  public function editparticipant(Request $request){
    $table = $request->session()->get("table") ?? "panelists";
    $event_id = $request->session()->get("event_id") ?? 1;
    $columns = json_decode(json_encode(\DB::select("DESCRIBE {$table}")),true);
    $columnstr = '';
    $valuestr='';
    foreach($columns as $row){
      if($row['Field'] != 'id' && $row['Field'] != 'event_id'){
        if(strpos($row['Type'], 'int') == 0){
          $valuestr .= "{$row['Field']} = '" . $request->input($row['Field']) . "',";
        }
        else{
          $valuestr .= "{$row['Field']} = " . $request->input($row['Field']) . ",";
        }
      }
    }
    $valuestr = substr($valuestr, 0, strlen($valuestr)-1);
    \DB::select("UPDATE {$table} SET {$valuestr} WHERE id = {$request->id}");
    return redirect(route("participants"))->with("success","Edited successfully");
  }
}
