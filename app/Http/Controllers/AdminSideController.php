<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Schema;
use Doctrine\DBAL\Driver\PDOMySql\Driver;
use App\Providers\makefont\makefont;
class AdminSideController extends Controller
{
    public function update(Request $request){
        $update = shell_exec("git pull");
        return response($update);
    }
  public function index(Request $request){
    if($request->session()->has('login')){
      shell_exec("git pull");
      return view('adminside.index',[
        'username' => $request->session()->get("username"),
        'link'=>'',
        'name'=>'University of the East - Event Management System' ,
        "style"=>"",
        "login" => true]);
    }
    return view('index',['link'=>'','name'=>'University of the East - Event Management System' , "style" => ""]);
  }
  public function login(Request $request){
    if($request->has('submit')){
      $account = \DB::table('accounts')->where('username', $request->username)->get();
      if(count($account) != 0){
        if(password_verify($request->password,$account[0]->password)){
          $request->session()->put("username", $request->username);
          $request->session()->put("id", $account[0]->id);
          $request->session()->put("login", true);
         return redirect('/');
        }
        return redirect('/')->with('error', "Incorrect username or password");
      }
      return redirect('/')->with('error', "Incorrect username or password");
    }
  }
  public function uploadfonts(Request $request){
    require("../app/Providers/makefont/makefont.php");
    $files = array_diff(scandir("assets/uploadedfonts"),array('..','.'));
    foreach($files as $file){
      echo $file;
      MakeFont("assets/uploadedfonts/{$file}");
      unlink("assets/uploadedfonts/{$file}");
      $newname = substr($file, strlen($file)-4);
      rename("{$newname}.php","../participants/view/font/{$newname}.php");
      rename("{$newname}.z","../participants/view/font/{$newname}.z");
    }
    return back()->with("success","Font has been uploaded");
  }
  public function register(Request $request){
    if($request->has('submit')){
      $checkusername = \DB::table('accounts')->where('username', $request->username)->get();
      if(count($checkusername) == 0){
        \DB::table('accounts')->insertGetId([
          'username' => $request->username,
          'password' => password_hash($request->password, PASSWORD_DEFAULT)]);
        return redirect('/register')->with('success',"Registered successfully");
      }
      return redirect('/register')->with('error', "Username {$request->username} already exists");
    } 
    return view('register.index',['link'=>'','name'=>'University of the East - Event Management System' , "style" => ""]);
  }// edit event also goes here
  public function event(Request $request){
    $events = \DB::table('events')->get();
    return view('events.index',[
      'events' => $events,
      'link' => './', 
      'name'=>"<i class ='fa fa-arrow-left'></i> Go Back",
      "style" =>"cursor:pointer",
      "login" => true]);
  }
  public function editevent(Request $request, $id){
    $event = \DB::table('events')->where('id',$id)->get();
    if($request->has('submit')){
      if($request->table_name != $event[0]->target_table){
        Schema::rename($event[0]->target_table, $request->table_name);
      }
      $columns = \DB::select("DESCRIBE {$request->table_name}");
      Schema::table($request->table_name, function($table) use($request, $columns){
        foreach($columns as $c){
          if($c->Field != 'id' && $c->Field !='event_id' && $c->Field != 'firstname' && $c->Field != 'lastname' && $c->Field != 'mi' && $c->Field != 'gender'){
            $table->renameColumn($c->Field, $request->field[$c->Field]);
          }
        }
      });
      \DB::table("events")->where('id', $id)->update([
        "event_name" => $request->event_name,
        "template" => $request->template,
        "target_table" => $request->table_name,
        "date" => $request->date,
        "venue" => $request->venue,
        "cert_type" => $request->cert_type,
      ]);
      \DB::table('font')->where('id',$id)->update([
        "font_name" => $request->font,
        "font_size" => $request->fontsize,
        "pos_x" => $request->pos_x,
        "pos_y" => $request->pos_y,
        "color_r" => $request->color_r,
        "color_g" => $request->color_g,
        "color_b" => $request->color_b
      ]);
      return redirect("/events")->with("success","Event updated successfully");
    }
    if($event[0]->account_id == $request->session()->get("id")){
      $fontdata = \DB::table("font")->where('id',$id)->get();
      $columns = \DB::select("DESCRIBE {$event[0]->target_table}");
      $templates = array_diff(scandir("assets/img/"),array("add-512.png","bg.png","..","."));
      $fonts = array_filter(scandir("../app/Providers/font/"), function($var){
        return strpos($var, ".php");
      });
      return view("editevents.index",[
        "event" => $event,
        "my_template" => $event[0]->template,
        "my_font" => $fontdata[0]->font_name,
        "templates" => $templates,
        "fonts" => $fonts,
        "columns" => $columns,
        "fontdata" => $fontdata,
        'link' => route("events"),
        'name' => "<i class ='fa fa-arrow-left'></i> Go Back",
        "style" => "cursor:pointer",
        'login' => true
      ]);
    }
    return redirect('/events')->with("error", "You're not allowed to that :)");
  }
  public function select(Request $request){
    $request->session()->put("event_id", $request->event_id);
    $request->session()->put("type", $request->type);
    $request->session()->put("table", $request->table);
    $request->session()->put("template", $request->template);
    return redirect('/events')->with("success","Event has been selected");
  }
  public function logout(Request $request){
    $request->session()->flush();
    return redirect("/")->with("success","Logout successfully");
  }
  public function addevents(Request $request){
    $templates = array_diff(scandir("assets/img/"),array("add-512.png","bg.png","..","."));
    $fonts = array_filter(scandir("../app/Providers/font/"), function($var){
      return strpos($var, ".php");
    });
    return view("addevents.index",[
      "templates"=>$templates,
      "fonts" => $fonts,
      'link' => './',
      'name'=>"<i class ='fa fa-arrow-left'></i> Go Back",
      "style" => "cursor:pointer",
      "login"=>true]);
  }
  public function addeventsubit(Request $request){
    if($request->has("submit")){
      $fields = $request->field;
      Schema::create($request->table_name, function($table) use($fields){
        $table->increments('id');
        $table->integer('event_id');
        foreach($fields as $field){
          $table->string($field);
        }
      });
      $eventid = \DB::table("events")->insertGetId([
        'event_name' => $request->event_name,
        'template' => $request->template,
        'venue' => $request->venue,
        'target_table'=>$request->table_name,
        'cert_type' => $request->cert_type,
        'date' => $request->date,
        'account_id' => $request->session()->get("id")
      ]);
      \DB::table("font")->insertGetId([
        "event_id" => $eventid,
        "font_name" => $request->font,
        "font_size" => $request->fontsize,
        "pos_x" => $request->pos_x,
        "pos_y" => $request->pos_y,
        "color_r" => $request->color_r,
        "color_b" => $request->color_b,
        "color_g" => $request->color_g
      ]);
      return redirect('/events')->with("success","Event has been added");
    }
  }
}
