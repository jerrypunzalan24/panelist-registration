<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Providers\FPDF;
use App\Providers\makefont;
class PrintController extends Controller
{
  public function preview(Request $request){
    $fpdf = new FPDF();
    $fpdf->AddPage(strtoupper($request->cert_type[0]));
    $font_name ='';

    if($request->font != 'Arial'){
      $font_name ="Custom";
      $fpdf->AddFont($font_name, "", "{$request->font}.php");
    }
    else{
      $font_name = $request->font;
    }
    $fpdf->Image("assets/img/{$request->template}/m-default.jpg",0,0,$fpdf->GetPageWidth(), $fpdf->GetPageHeight());
    $fpdf->SetFont($font_name, "", $request->fontsize);
    $fpdf->SetXY($request->pos_x, $request->pos_y);
    $fpdf->SetTextColor($request->r,$request->g,$request->b);
    $fpdf->Cell(0,10,"Sample text",0,0,'C');
    $fpdf->Output();
  }
  public function certificates(Request $request){
    $fpdf = new FPDF();
    $table = $request->session()->get("table");
    $id = $request->session()->get('event_id')  ?? 1;
    $participants = \DB::table($table)->where('event_id',$id)->orderBy("id","DESC")->get();
    $event = \DB::table('events')->where("id", $id)->get();
    $font = \DB::table('font')->where("event_id",$event[0]->id)->get();
    $type = $event[0]->cert_type =="landscape" ? "L" : "P";
    $fpdf->AddPage($type);
    $font_name = "";
    if($font[0]->font_name != "Arial"){
      $font_name = "Custom";
      $fpdf->AddFont($font_name,'',"{$font[0]->font_name}.php");
    }
    else{
      $font_name = $font[0]->font_name;
    }
    $count = 1;
    $currentyear = date("Y");
    foreach($participants as $row){
      $g = strtolower($row->gender);
      if($event[0]->cert_type == 'portrait'){
        $fpdf->Image("assets/img/{$event[0]->template}/{$g}-default.jpg",0,0,$fpdf->GetPageWidth(),$fpdf->GetPageHeight());
        $fpdf->SetXY(-45,0);
        $fpdf->SetFont("Arial",'B',12);
        $padnum = str_pad($count,3,'000', STR_PAD_LEFT);
        $fpdf->SetTextColor(0,0,0);
        $fpdf->Cell(0,10,"ccssrnd-{$event[0]->template}{$currentyear}-{$padnum}",0,0,'C');
        $fpdf->SetXY($font[0]->pos_x,$font[0]->pos_y);
        if(trim($row->mi) === ''){
          $fullname = strtoupper($row->firstname) . " " .  strtoupper($row->lastname);
        }
        else{
          $fullname = strtoupper($row->firstname) ." ".  strtoupper($row->mi) . ". " . strtoupper($row->lastname);
        }
        if(strlen($fullname) > 24){
          $fpdf->SetFont($font_name,"",$font[0]->font_size - (strlen($fullname) /3));
        }
        else{
          $fpdf->SetFont($font_name,"",$font[0]->font_size);
        }
        $fpdf->SetTextColor($font[0]->color_r, $font[0]->color_g, $font[0]->color_b);
        $fpdf->Cell(0,10,$fullname,0,0,'C');
        $fpdf->AddPage($type);
        $count++;
      }
      else if($event[0]->cert_type=='landscape'){
        $fpdf->Image("assets/img/{$event[0]->template}/{$g}-default.jpg",0,0,$fpdf->GetPageWidth(), $fpdf->GetPageHeight());
        $fpdf->SetXY(2,0);
        $fpdf->SetFont("Arial","B",12);
        $padnum = str_pad($count, 3, '000', STR_PAD_LEFT);
        $fpdf->SetTextColor(0,0,0);
        $fpdf->Cell(0,10,"ccssrnd-{$event[0]->template}{$currentyear}-{$padnum}",0,0,'R');
        $fpdf->SetXY($font[0]->pos_x,$font[0]->pos_y);
        if(trim($row->mi) === ''){
          $fullname = strtoupper($row->firstname) . " " .  strtoupper($row->lastname);
        }
        else{
          $fullname = strtoupper($row->firstname) ." ".  strtoupper($row->mi) . ". " . strtoupper($row->lastname);
        }
        if(strlen($fullname) > 24){
          $fpdf->SetFont($font_name, "",$font[0]->font_size - (strlen($fullname) / 3));
        }
        else{
          $fpdf->SetFont($font_name,"",$font[0]->font_size);
        }
        $fpdf->SetTextColor($font[0]->color_r, $font[0]->color_g, $font[0]->color_b);
        $fpdf->Cell(0,10, $fullname, 0,0,'C');
        $fpdf->AddPage($type);
        $count++;
      }
    }
    $fpdf->Output();
  }
  public function lists(Request $request){
    $id = $request->session()->get('event_id')  ?? 1;
    $event = \DB::table('events')->where("id", $id)->get();
    $participants = \DB::table($request->session()->get("table"))->orderBy("id","DESC")->get();
    $fpdf = new FPDF();
    $fpdf->AddPage("L");
    $fpdf->SetFont("Arial","B",11);
    $fpdf->Cell(270,10,"List of participants",0,0,"C");
    $fpdf->Ln();
    $fpdf->Cell(200,10,"Event name: {$event[0]->event_name}");
    $currentday = date("F d, Y", strtotime($event[0]->date));
    $fpdf->Cell(100,10, "Event held: {$currentday}");
    $fpdf->Ln();
    $fpdf->Cell(100,10,"Venue: {$event[0]->venue}");
    $fpdf->Ln();
    $fpdf->Cell(75,10,"Full name",1);
    $fpdf->Cell(100,10,"School",1);
    $fpdf->Cell(80,10,"Position",1);
    $fpdf->Ln();
    $fpdf->SetFont("Arial","",10);
    foreach($participants as $row){
      $fpdf->Cell(75,10,"{$row->lastname}, {$row->firstname} {$row->mi}.");
      $fpdf->Cell(100,10,$row->school);
      $fpdf->Cell(80,10,$row->position);
      $fpdf->Ln();
    }
    $fpdf->Output();
  }
}
