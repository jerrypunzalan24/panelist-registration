-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Sep 27, 2018 at 05:06 AM
-- Server version: 10.1.25-MariaDB
-- PHP Version: 7.1.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `registration_panelist`
--

-- --------------------------------------------------------

--
-- Table structure for table `accounts`
--

CREATE TABLE `accounts` (
  `id` int(11) NOT NULL,
  `username` varchar(50) NOT NULL,
  `password` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `accounts`
--

INSERT INTO `accounts` (`id`, `username`, `password`) VALUES
(1, 'rnd', '$2y$10$VijKYHDqZN3LWTfVA2uSS.BC9i3l2vzMoaVn3ZcBjFyDP5BWB0OAi');

-- --------------------------------------------------------

--
-- Table structure for table `events`
--

CREATE TABLE `events` (
  `id` int(11) NOT NULL,
  `event_name` varchar(255) NOT NULL,
  `template` varchar(30) NOT NULL,
  `date` datetime NOT NULL,
  `venue` varchar(30) NOT NULL,
  `cert_type` varchar(15) NOT NULL,
  `account_id` int(11) NOT NULL,
  `target_table` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `events`
--

INSERT INTO `events` (`id`, `event_name`, `template`, `date`, `venue`, `cert_type`, `account_id`, `target_table`) VALUES
(1, 'Thesis Defense', 'thesis', '2018-09-22 07:00:00', 'CCSS Lobby', 'portrait', 1, 'panelists'),
(2, 'Asking the better questions', 'askingthebetter', '2018-09-26 00:00:00', 'MPH', 'landscape', 1, 'participants'),
(3, 'Do less. be more: living a purposeful college life', 'doless', '2018-09-27 00:00:00', 'MPH', 'landscape', 1, 'participants'),
(4, 'Prof Seminar', 'prof-seminar', '2018-09-27 00:00:00', 'MPH', 'landscape', 1, 'participants');

-- --------------------------------------------------------

--
-- Table structure for table `font`
--

CREATE TABLE `font` (
  `id` int(11) NOT NULL,
  `event_id` int(11) NOT NULL,
  `font_name` varchar(30) NOT NULL,
  `font_size` int(11) NOT NULL,
  `pos_x` int(11) NOT NULL,
  `pos_y` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `font`
--

INSERT INTO `font` (`id`, `event_id`, `font_name`, `font_size`, `pos_x`, `pos_y`) VALUES
(1, 1, 'Cambria', 40, 20, 150),
(2, 2, 'Arial', 40, 20, 105),
(3, 3, 'Arial', 40, 20, 110),
(4, 4, 'Arial', 40, 20, 105);

-- --------------------------------------------------------

--
-- Table structure for table `panelists`
--

CREATE TABLE `panelists` (
  `id` int(11) NOT NULL,
  `firstname` varchar(30) NOT NULL,
  `lastname` varchar(30) NOT NULL,
  `mi` varchar(4) NOT NULL,
  `email` varchar(64) NOT NULL,
  `company` varchar(255) NOT NULL,
  `contact` varchar(11) NOT NULL,
  `gender` varchar(1) NOT NULL,
  `event_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `panelists`
--

INSERT INTO `panelists` (`id`, `firstname`, `lastname`, `mi`, `email`, `company`, `contact`, `gender`, `event_id`) VALUES
(3, 'Geecee Maybelline', 'Manabat', 'A', 'geecee.manabat@ue.edu.ph', 'University of the East', '09166278942', 'F', 1),
(4, 'Roy', 'Callope', 'B', 'rbcallope@gmail.com', 'University of the East', '09209463906', 'M', 1),
(5, 'Janusvielle', 'Zate', 'A', 'janus.aragones@gmail.com', 'University of the East', '09178666472', 'F', 1),
(6, 'Kristian', 'Gonzales', 'S', 'kristian.s.gonzales@accenture.com', 'Accenture', '09985946215', 'M', 1),
(7, 'Mary Grace', 'Magcale - Moreno', 'M', 'marygrace.moreno@arrow.com', 'Outsourced - Arrow ECS ANZ', '09088209070', 'F', 1),
(8, 'Ariel', 'Aviles', 'D', 'CO113@YAHOO.COM', 'University of the East ', '09154668888', 'M', 1),
(9, 'Paul Gilbert', 'Maglaya', 'V', 'GILNICX_2000@YAHOO.COM', 'University of the East', '09271509953', 'M', 1),
(10, 'Mary Grace', 'Ventura', 'G', 'marygrace.ventura@ue.edu.ph', 'University of the East', '0905471772', 'F', 1),
(11, 'Charles Candido', 'Cammayo', 'M', 'CHARLES.CAMMAYO@ALORICA.COM', 'ALORICA PHILIPPINES INC.', '09327957354', 'M', 1),
(12, 'Pauline', 'Milan', 'A', 'pauline.milan@sageninja.com', 'Sage Ninja Creatives Co', '09177062884', 'F', 1),
(13, 'Mark Kristoffer', 'Pradillada', 'A', 'mpradillada@gmail.com', 'Accenture Inc.', '09174646255', 'M', 1),
(14, 'Kimberly', 'Trinidad', 'M', 'kimberlymtrinidad@yahoo.com', 'TERADATA PHILIPPINES INC.', '09778104598', 'F', 1),
(15, 'Krystel Gayle', 'Manzanilla', 'M', 'gayle.manzanilla@gmail.com', 'Aurecon', '09777233130', 'F', 1),
(16, 'Everard', 'Onggon', 'C', 'everard@thenextjump.com', 'The Next Jump', '09062831310', 'M', 1),
(17, 'Sherwine', 'Cordero', 'A', 'whine23@gmail.com', 'Trend Micro Inc.', '09277991153', 'F', 1),
(19, 'Ashraf', 'Encila', 'N', 'ashEncila@gmail.com', 'Arch Global Services Inc.', '09176345665', 'M', 1),
(20, 'Kimberly Ann', 'Raymundo', 'F', 'raymundo.kiann@gmail.com', 'Nowcom Global Services, LLC', '09173268227', 'F', 1),
(21, 'Ma. Teresa', 'Borebor', 'F', 'teresa.borebor@ue.edu.ph', 'University of the East', '09178585011', 'F', 1),
(22, 'Lea', 'Pasion', 'C', 'PASION.LEA@BDO.COM.PH', 'BANCO DE ORO UNIBANK INC.', '09253031973', 'F', 1),
(23, 'Jan Rino', 'Sabile', 'T', 'janrinosabile@gmail.com', 'SMS GLOBAL TECHNOLOGIES INC.', '09151824129', 'M', 1),
(25, 'Carl Louise', 'McDonough', 'B', 'carllouisemcdonough08@gmail.com', 'Schneider Electric Asia Ltd.', '09175958148', 'M', 1),
(26, 'Roselyn', 'Alcantara', 'I', 'roselynalcantara@gmail.com', 'E-Tech Business Solutions', '09266807692', 'F', 1),
(27, 'Lionell', 'Libarios', 'H', 'lionelllibarios12@yahoo.com', 'RisingTide Mobile Inc.', '09175027146', 'M', 1),
(28, 'Eugene', 'Constantino', 'E', 'dh.eugeneconstantino@gmail.com', 'Digital Hog Online Marketing Services Inc.', '09163009286', 'M', 1),
(29, 'Roselle', 'Basa', 'S', 'roselle.basa@ue.edu.ph', 'University of the East', '09177949329', 'F', 1),
(31, 'Marivic', 'Gatus', 'M', 'marivicgatus@ue.edu.ph', 'University of the East', '09353006470', 'F', 1),
(32, 'Katrine Mae', 'Alba', 'C', 'katrinemaealba@gmail.com', 'FedEx', '09086092094', 'F', 1),
(33, 'Theresse', 'Garcera', 'G', 'garceratheresse@yahoo.com', 'Alorica', '09055655684', 'F', 1),
(34, 'Bryan', 'Oab', 'D', 'bryan.oab25@gmail.com', 'Accenture - Avanade', '63916336867', 'M', 1),
(35, 'Julie Anne', 'Polican', 'S', 'julieannepolican@gmail.com', 'CheQ Systems', '09566092203', 'F', 1),
(36, 'Mark Anthony', 'Uy', 'D', 'markanthony.uy@ue.edu.ph', 'University of the East - Manila', '09184314710', 'M', 1),
(37, 'Anna Jane', 'Matillano', 'O', 'aomatillano@gmail.com', 'Salarium', '09283912143', 'F', 1),
(38, 'Marita', 'Tolentino', 'N', 'marita.tolentino@ue.edu.ph', 'University of the East', '09167298937', 'F', 1),
(39, 'Ma. Ymelda', 'Batalla', 'C', 'maymelda.batalla@ue.edu.ph', 'University of the East', '09279725095', 'F', 1),
(40, 'Melie Jim', 'Sarmiento', 'F', 'mjfsarmiento@ahoo.com', 'University of the East', '09159128040', 'F', 1),
(41, 'Mary Joyce', 'Rillon', 'G', 'joyce_rillon@yahoo.com', 'Oracle Philippines', '63917113752', 'F', 1),
(42, 'Dionisio', 'Miguel Jr.', 'L', 'donlmiguel@gmail.com', 'BDO Unibank, Inc.', '09167671395', 'M', 1),
(43, 'Arne', 'Bana', 'R', 'arne.bana@ue.edu.ph', 'University of the East - Manila', '09178978959', 'M', 1),
(44, 'Ralph Alvin', 'Almeda', 'W', 'rwalmeda15@gmail.com', 'BPI', '09052700517', 'M', 1),
(45, 'Miguel Angelo', 'Tolentino', 'A', 'miguelangelo.tolentino@ue.edu.ph', 'University of the East', '09175427318', 'M', 1),
(46, 'Reynaldo', 'Zausa', 'B', 'reyzausa@gmail.com', 'Cheq Systems Inc. ', '09178826204', 'M', 1),
(47, 'Reynaldo Gerald', 'Endaya II', 'T', 'reynaldogeraldendaya@yahoo.com', 'Shang Software Solutions Inc.', '09278610291', 'M', 1),
(48, 'Henry Dyke', 'Balmeo', 'A', 'henrydyke@yahoo.com', 'University of the East - IT Department', '09178405165', 'M', 1),
(49, 'Marc Rodin', 'Ligas', 'C', 'marcrodin.ligas@ue.edu.ph', 'University of the East', '09055926873', 'M', 1),
(50, 'Alvin', 'Gutierrez', 'T', 'alvingjohndrich@gmail.com', 'Bank Of Tokyo MUFG', '9285034660', 'M', 1),
(51, 'May Greeze', 'de Zosa', 'M', 'maygreeze.dezosa@ue.edu.ph', 'University of the East', '09086111856', 'F', 1),
(52, 'Christalyn', 'Maramag', 'B', 'cristal.maramag@gmail.com', 'AXA Philippines', '09278838818', 'F', 1),
(53, 'Rudyard', 'Pesimo', 'C', 'rcpesimo@gmail.com', 'University of the East - Manila', '09771005101', 'M', 1),
(55, 'Andrei Ramases', 'Quitalig', 'B', 'andrei_quitalig@yahoo.com', 'SAP Philippines', '09178247742', 'M', 1),
(56, 'Isaac', 'Mongcal', 'Z', 'isaac.mongcal@gmail.com', 'University of the East', '09355003034', 'M', 1),
(57, 'Raymond', 'Valdez', 'E', 'esper21gi@gmail.com', 'Moon Eaters Studio', '09162022178', 'M', 1),
(58, 'Felicito', 'Nuarin', 'S', 'chito.nuarin@ue.edu.ph', 'University of the East', '09199927322', 'M', 1),
(59, 'Jessa Marie', 'Bernardo', 'E', 'jessamebernardo@gmail.com', 'Willis Towers Watson', '09162090981', 'F', 1),
(60, 'Reynalyn', 'Elon', 'T', 'reynalyn.elon@ue.edu.ph', 'University of the East', '09958964923', 'F', 1),
(62, 'Julie Vernes', 'Rodriguez', 'C', 'julievernes.rodriguez@gmail.com', 'Accenture, Inc.', '09175734141', 'F', 1),
(63, 'Ferdinand', 'Hipolito', 'A', 'ferdie.hipolito@toyotanorthedsa.com', 'Toyota North EDSA', '09951729164', 'M', 1),
(64, 'Mark Anthony', 'Galang', 'S', 'makgalang@gmail.com', 'Capgemini Philippines', '09271529042', 'M', 1),
(65, 'Brenda', 'Basas', 'F', 'basasbrenda@gmail.com', 'GVC', '09773289800', 'F', 1),
(66, 'Cynirene', 'Carillo', 'C', 'cynairenecc@yahoo.com', 'Sykes Asia Inc.', '09989655498', 'F', 1),
(67, 'Elijah', 'Kayguan', 'V', 'elijah.kayguan@yahoo.com', 'Shang Software Solutions Inc.', '09154716520', 'M', 1),
(68, 'Bryan Carlos', 'Andico', 'A', 'andicobryan@gmail.com', 'Unilab Inc.', '09772313977', 'M', 1),
(69, 'Bryand Gabriel', 'Rasco', 'M', 'bryce2006@gmail.com', 'UE', '09052596245', 'M', 1),
(70, 'Benjamin', 'Maure', 'E', 'benjiemaure@gmail.com', 'Xentrix Toons Inc.', '09178755778', 'M', 1),
(71, 'Ronald', 'Orcino', 'R', 'mhacdho@yahoo.com', 'Emerson Electric ASIA ROHQ', '09178415339', 'M', 1),
(72, 'Maria Elena', 'Valenzuela', 'P', 'elaine5ph@yahoo.com', 'National Irrigation Administration', '09225309180', 'F', 1),
(73, 'Pierre', 'Carag', 'C', 'apoi_carag@yahoo.com', 'Bank of the Philippine Islands', '09209079091', 'M', 1),
(74, 'Nathex-Kasem', 'Iwuanyanwu Jr', 'F', 'iwuanyanwu_nathex@yahoo.com', 'Oracle Philippines', '09175985053', 'M', 1),
(75, 'Edmon', 'Torres', 'L', 'edmon.torres@ue.edu.ph', 'University of the East', '09957010715', 'M', 1),
(76, 'Johnny', 'Hermoso', 'B', 'jhoy_28@yahoo.com', 'University of the East', '09391086830', 'M', 1),
(77, 'Jerry', 'Punzalan', 'f', 'jerrypunzalan24@gmail.com', 'wqeqwe', '9156155821', 'M', 1),
(78, 'Jerryyyy', 'Punzalannnn', 'f', 'jerrypunzalan24@gmail.com', 'qweqwewqe', '09156155821', 'M', 1);

-- --------------------------------------------------------

--
-- Table structure for table `participants`
--

CREATE TABLE `participants` (
  `id` int(11) NOT NULL,
  `event_id` int(11) NOT NULL,
  `firstname` varchar(50) NOT NULL,
  `lastname` varchar(50) NOT NULL,
  `mi` varchar(4) NOT NULL,
  `gender` varchar(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `participants`
--

INSERT INTO `participants` (`id`, `event_id`, `firstname`, `lastname`, `mi`, `gender`) VALUES
(4, 2, 'Laurenz', 'Melosantos', 'S', 'M'),
(5, 2, 'Rizalyna Angellie', 'Ramos', 'G', 'F'),
(6, 2, 'Christian Noel', 'Benitez', 'F', 'M'),
(7, 2, 'Grix Romney', 'Aquino', 'B', 'M'),
(8, 2, 'Ivan Miguel', 'Escala', 'L', 'M'),
(9, 2, 'Deo Rico', 'Adel', 'A', 'M'),
(10, 2, 'John Angelo', 'Estonactoc', 'D', 'M'),
(11, 2, 'Jan Andrei', 'Cruz', 'L', 'M'),
(12, 2, 'Matt Daniel', 'Cajucom', 'G', 'M'),
(13, 2, 'Bhenz Danielle', 'Espiritu', 'M', 'M'),
(14, 2, 'Angel Lynn', 'Sabater', 'B', 'F'),
(15, 2, 'Jerome', 'Alejo', 'D', 'M'),
(16, 2, 'John Paolo', 'Clitar', 'A', 'M'),
(17, 2, 'Frankie Mae', 'Castor', 'H', 'F'),
(18, 2, 'Karen Joy', 'Delfin', 'B', 'F'),
(19, 2, 'Christian Jed', 'Del Rosario', 'E', 'M'),
(20, 2, 'Treb Miguel', 'Manglangit', 'D', 'M'),
(21, 2, 'Angelito', 'Castro', 'L', 'M'),
(22, 2, 'Denzel Roi Anthony', 'Ealunton', 'A', 'M'),
(23, 2, 'Manny Alejandro', 'Young', 'A', 'M'),
(24, 2, 'John Christian', 'Martinez', 'N', 'M'),
(25, 2, 'Mavelyn', 'Fatalla', 'C', 'F'),
(26, 2, 'Mary Grace', 'Manalo', 'M', 'F'),
(27, 2, 'Sweet Nicole', 'Pajares', 'N', 'F'),
(28, 2, 'Micaela', 'Delmonte', 'R', 'F'),
(29, 2, 'Enrico', 'Galido', 'B', 'M'),
(30, 2, 'Omar', 'Labio', 'D', 'M'),
(31, 2, 'Randell', 'Tan', 'Z', 'M'),
(32, 2, 'Rufina', 'Arroyo', 'S', 'F'),
(33, 2, 'Sandra', 'Castillo', 'P', 'F'),
(34, 2, 'Julius Glenn', 'Espiritu', 'J', 'M'),
(35, 2, 'Dick Arnes', 'Boco', 'G', 'M'),
(36, 2, 'Nienburg', 'Arizala', 'D', 'M'),
(37, 2, 'Cristian', 'Nadela', 'C', 'M'),
(38, 2, 'Jose Carlo', 'Javate', 'S', 'M'),
(39, 2, 'Roderick', 'Mission', 'A', 'M'),
(40, 2, 'Levinyl', 'Villanueva', 'M', 'M'),
(41, 2, 'Elias', 'Deveza III', 'M', 'M'),
(42, 2, 'Marc Danzel', 'Gervacio', 'C', 'M'),
(43, 2, 'James Merville', 'Tura', 'A', 'M'),
(44, 2, 'Bien Carlo', 'Bobis', 'R', 'M'),
(45, 2, 'Kyle', 'Dansalan', 'S', 'M'),
(46, 2, 'Miracris', 'Lipata', 'I', 'F'),
(47, 2, 'Daniela Marie', 'Recreo', 'L', 'F'),
(48, 2, 'Tristan Dave', 'Regulacion', 'M', 'M'),
(49, 2, 'Jones Mark', 'Pioquinto', 'S', 'M'),
(50, 2, 'Sherwin Paul', 'Moceros', 'S', 'M'),
(51, 2, 'Jose Rudolfo', 'Tarrosa', 'A', 'M'),
(52, 2, 'Ralf Brian', 'Aranton', 'S', 'M'),
(53, 2, 'Alyanna Mariz', 'Cantos', 'C', 'F'),
(54, 2, 'Maryfrietz Nicole', 'Daganato', 'S', 'F'),
(55, 2, 'Mark Jefferson', 'Calma', 'D', 'M'),
(56, 2, 'Bea Jewel', 'Vines', 'C', 'F'),
(57, 2, 'Waleed Ahmed', 'Sultan', '', 'M'),
(58, 2, 'Darius Peter', 'Ariate', 'N', 'M'),
(59, 2, 'Rey Ann Timothy', 'Tan', 'I', 'M'),
(60, 2, 'Abdul Rauf', 'Dimaampao', 'S', 'M'),
(61, 2, 'Lorence Louise', 'Pangan', 'M', 'M'),
(62, 2, 'John Christian', 'Bagay', 'P', 'M'),
(63, 2, 'Neil Albert', 'Segarino', 'G', 'M'),
(64, 2, 'Francis Louie', 'Torculas', 'A', 'M'),
(65, 2, 'Kenneth', 'Pinto', 'P', 'M'),
(66, 2, 'Ulyses Edcel', 'Adrales', 'C', 'M'),
(67, 2, 'Mark Angelo', 'Ramos', 'S', 'M'),
(68, 2, 'Miguel Paolo', 'Rodriguez', '', 'M'),
(69, 2, 'Andrei Lloyd Pocholo', 'Gatchalian', 'A', 'M'),
(70, 2, 'Princes Lovely', 'Aranda', 'L', 'F'),
(71, 2, 'Charles Anthony', 'Mendoza', 'T', 'M'),
(72, 2, 'Mikko', 'Ulanday', 'C', 'M'),
(73, 2, 'Luis Eljerald', 'Abuda', 'S', 'M'),
(74, 2, 'Patrick', 'Labadia', 'C', 'M'),
(75, 3, 'sarmiento', 'melie jim', 'f', 'M'),
(76, 3, 'May Greeze', 'de Zosa', 'M', 'F'),
(77, 3, 'Mary Grace', 'Ventura', 'G.', 'F'),
(78, 3, 'RUDYARD', 'PESIMO', 'C', 'M'),
(79, 3, 'Ma. Teresa', 'Borebor', 'F.', 'F'),
(80, 3, 'Ma. Ymelda', 'Batalla', 'C', 'F'),
(81, 3, 'Bernadette', 'San Diego', 'E', 'F'),
(82, 3, 'Edmon', 'Torres', 'L.', 'M'),
(83, 4, 'Melie Jim', 'Sarmiento', 'F', 'F'),
(84, 4, 'May Greeze', 'De Zosa', 'M', 'F'),
(85, 4, 'Mary Grace', 'Ventura', 'G', 'F'),
(86, 4, 'Rudyard', 'Pesimo', 'C', 'M'),
(87, 4, 'Ma. Teresa', 'Borebor', 'F', 'F'),
(88, 4, 'Ma. Ymelda', 'Batalla', 'C', 'F'),
(89, 4, 'Bernadette', 'San Diego', 'E', 'F'),
(90, 4, 'Edmon', 'Torres', 'L', 'M'),
(91, 4, 'Dino-Ver', 'Federizo', 'A', 'M'),
(92, 4, 'Roy', 'Callope', 'B', 'M'),
(93, 4, 'Ariel', 'Aviles', 'D', 'M'),
(94, 4, 'Shirley', 'Moraga', 'D', 'F'),
(95, 4, 'Marita', 'Tolentino', '', 'F'),
(96, 4, 'Miguel', 'Tolentino', '', 'M'),
(97, 4, 'Reynalyn', 'Elon', '', 'F'),
(98, 4, 'Roselle', 'Basa', 'P', 'F'),
(99, 4, 'Mark Anthony', 'Uy', '', 'M');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `accounts`
--
ALTER TABLE `accounts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `events`
--
ALTER TABLE `events`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `font`
--
ALTER TABLE `font`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `panelists`
--
ALTER TABLE `panelists`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `participants`
--
ALTER TABLE `participants`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `accounts`
--
ALTER TABLE `accounts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `events`
--
ALTER TABLE `events`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `font`
--
ALTER TABLE `font`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `panelists`
--
ALTER TABLE `panelists`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=79;
--
-- AUTO_INCREMENT for table `participants`
--
ALTER TABLE `participants`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=100;COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
