<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::any("/register","AdminSideController@register");
Route::any('/',"AdminSideController@index")->name("home");
Route::post('/login', "AdminSideController@login");
Route::get("/update","AdminSideController@update");
Route::group(['prefix'=>'', 'middleware' => ['login_required']],function(){
  Route::get("/logout","AdminSideController@logout")->name("logout");
  Route::get("/events","AdminSideController@event")->name("events");
  Route::any('/events/edit/{id}', "AdminSideController@editevent")->where(['id'=>'[0-9]+']);
  Route::get("/events/preview","PrintController@preview")->name("preview");
  Route::get("/events/fontupload", "AdminSideController@uploadfonts")->name("fontupload");
  Route::post("/events/select","AdminSideController@select");
  Route::get("/addevents","AdminSideController@addevents")->name("addevents");
  Route::post("/addevents/submit", "AdminSideController@addeventsubit");
  Route::get("/registration","RegistrationController@index");
  Route::get("/registration/certificates","PrintController@certificates");
  Route::get("/registration/participants", "RegistrationController@participants")->name("participants");
  Route::post("/registration/participants/edit", "RegistrationController@editparticipant");
  Route::get("/registration/lists","PrintController@lists");
  Route::post("/registration/register","RegistrationController@addparticipant")->name("registerparticipant");
});
