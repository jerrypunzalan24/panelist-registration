<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class PanelistRegistration extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('accounts',function($table){
            $table->increments('id');
            $table->string('username');
            $table->string('password', 255);
        });
        \DB::table('accounts')->insert([
            'username' => 'rnd',
            'password' => password_hash('ueccssrnd', PASSWORD_DEFAULT)
        ]);
        Schema::create('events', function($table){
            $table->increments('id');
            $table->string('event_name',255);
            $table->string('template',30);
            $table->dateTime('date');
            $table->string('venue',30);
            $table->string('cert_type', 15);
            $table->integer('account_id');
            $table->string('target_table',30);
        });
        \DB::table('events')->insert(array(
            array(
                'event_name' => 'Thesis Defense',
                'template' => 'thesis',
                'date' => '2018-09-22 07:00:00',
                'venue' => 'CCSS Lobby',
                'cert_type' => 'portrait',
                'account_id' => 1,
                'target_table' => 'panelists',
            ),
            array(
                'event_name' => 'Asking the better questions',
                'template' => 'askingthebetter',
                'date' => '2018-09-26 00:00:00',
                'venue' => 'MPH',
                'cert_type' => 'landscape',
                'account_id' => 1,
                'target_table' => 'participants'
            ),
            array(
                'event_name' => 'Do less. be more: living a purposeful college life',
                'template' => 'doless',
                'date' => '2018-09-27 00:00:00',
                'venue' => 'MPH',
                'cert_type' => 'landscape',
                'account_id' => 1,
                'target_table' => 'participants'
            ),
            array(
                'event_name' => 'Prof Seminar',
                'template' => 'prof-seminar',
                'date' => '2018-09-27 00:00:00',
                'venue' => 'MPH',
                'cert_type' => 'landscape',
                'account_id' => 1,
                'target_table' => 'participants'
            ),
        ));
        Schema::create('font',function($table){
            $table->increments('id');
            $table->integer('event_id');
            $table->string('font_name');
            $table->integer('font_size');
            $table->integer('pos_x');
            $table->integer('pos_y');
            $table->integer('color_r');
            $table->integer('color_g');
            $table->integer('color_b');
        });
        \DB::table('font')->insert(array(
            array(
                'event_id' => 1,
                'font_name' => 'Cambria',
                'font_size' => 40,
                'pos_x' => 20,
                'pos_y' => 150,
                'color_r' => 0,
                'color_g' => 0,
                'color_b' => 0
            ),
            array(
                'event_id' => 2,
                'font_name' => 'Arial',
                'font_size' => 40,
                'pos_x' => 20,
                'pos_y' => 105,
                'color_r' => 0,
                'color_g' => 0,
                'color_b' => 0
            ),
            array(
                'event_id' => 2,
                'font_name' => 'Arial',
                'font_size' => 40,
                'pos_x' => 20,
                'pos_y' => 110,
                'color_r' => 0,
                'color_g' => 0,
                'color_b' => 0
            ),
            array(
                'event_id' => 2,
                'font_name' => 'Arial',
                'font_size' => 40,
                'pos_x' => 20,
                'pos_y' => 105,
                'color_r' => 0,
                'color_g' => 0,
                'color_b' => 0
            )
        ));
        Schema::create('panelists', function($table){
            $table->increments('id');
            $table->string('firstname',30);
            $table->string('lastname',30);
            $table->string('mi',4);
            $table->string('email',64);
            $table->string('company', 255);
            $table->string('contact', 11);
            $table->string('gender',1);
            $table->integer('event_id');
        });
        Schema::create('participants', function($table){
            $table->increments('id');
            $table->integer('event_id');
            $table->string('firstname',50);
            $table->string('lastname',50);
            $table->string('mi',4);
            $table->string('gender',1);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
