@include('../header');  
<div class ='card' style ='margin:10%; border-radius:0px;border:none;box-shadow:3px 3px 1px #444444'>
  <div class ='card-header' style ='background-color:#00C47D;margin-bottom:20px;border-radius:0px' align ='center'>
    <h1 style ='color:white;text-shadow:2px 2px 1px #444444'>LIST OF PARTICIPANTS
    </h1>
  </div>
  <div class ='card-body'>
    <div class ='row'>
      <div class ='col-sm-1' >
        <div style ='padding: 20px 20px 20px 0px'>
          <button class ='btn btn-success btn-custom' onclick ="window.location='certificates'">Print all
          </button>
        </div>
      </div>
      <div class ='col-sm-2'>
        <div style ='padding:20px 20px 20px 0px'>
          <button class ='btn btn-success btn-custom' onclick ="window.location ='lists'">Print list of participants
          </button>
        </div>
      </div>
    </div>
    <table class ='table' >
      <thead>
        @foreach($columns as $row)
          @if($row->Field != 'id')
            <th style ='color:white;background-color:#22bbbb;border-bottom-style:none;text-align:center;'>{{$row->Field}}</th>
          @endif
        @endforeach
        <th style ='color:white;background-color:#22bbbb;border-bottom-style:none;text-align:center'>Action</th>
      </thead>
      <tbody>
        @foreach($participants as $rows)
          <tr>
          @foreach($rows as $key => $row)
           @if($key!='id')
            <td id ='{{$key}}' style ='text-align:center;'>{{$row}}</td>
            @endif
          @endforeach
          <td>
            <button class ='btn btn-success btn-block btn-custom ' onclick = "editentry({{$rows->id}}, this)">Edit
            </button>
          </tr>
      @endforeach
      </tbody>
    </table>
  </div>
</div>
<!--Edit modal -->
<div class="modal fade" style ='border-radius:0px' id ='edit' tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header" style ='background-color:#0098BE'>
        <h5 class="modal-title" style ='color:white'>Edit modal
        </h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true" style ='color:white'>&times;
          </span>
        </button>
      </div>
      <div class="modal-body">
        <form method= 'post' action ='{{url()->current()}}/edit'>
          @csrf
          <div class ='form-group'>
            <div class ='row'>
              <input type ='hidden' name ='id'>
              <div class ='col-md-5'>
                <input type ='text' name ='lastname'  class ='form-control remove-border' placeholder ='Lastname' REQUIRED/>
              </div>
              <div class ='col-md-5'>
                <input type ='text' name ='firstname' class ='form-control remove-border' placeholder ='Firstname' REQUIRED/>
              </div>
              <div class ='col-md-2'>
                <input type ='text' name ='mi' maxlength="4" class ='form-control remove-border' placeholder ='M.I' REQUIRED/>
              </div>
            </div>
          </div>
          @foreach($columns as $row) 
              @if($row->Field != 'id' && $row->Field != 'firstname' && $row->Field != 'lastname' && $row->Field != 'mi' && $row->Field != 'event_id')
            @if(strpos($row->Field,'int') == 0)
          <div class ='form-group'>
            <input type ='text' name ='{{$row->Field}}' class ='form-control remove-border' placeholder ='{{ucfirst($row->Field)}}' REQUIRED/>
          </div>
          @else 
             <div class ='form-group'>
            <input type ='number' name ='{{$row->Field}}' class ='form-control remove-border' placeholder ='{{ucfirst($row->Field)}}' REQUIRED/>
          </div>
          @endif
        @endif 
        @endforeach
        </div>
        <div class="modal-footer" style ='margin-top:25px'>
          <input type ='submit' name ='submit' class="btn btn-success btn-custom" value ='Edit'>       
        </form>
      </div>
    </div>
  </div>
</div>
<script src ='{{asset("/assets/js/jquery-3.2.1.min.js")}}'>
</script>
<script src ='{{asset("/assets/js/bootstrap.min.js")}}'>
</script>
<script>
  function editentry(id, z){
    @foreach($columns as $row)
      @if($row->Field != 'id')
    var {{$row->Field}} = $(z).closest('tr').find('#{{$row->Field}}').html()
    $('input[name={{$row->Field}}]').val({{$row->Field}})
    @endif
  @endforeach
  $('input[name=id]').val(id)
    $('#edit').modal('show')
  }
</script>