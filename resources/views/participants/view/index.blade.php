<?php    
include_once('../../db.php');
include('fpdf.php');
if(isset($_GET['all'])){
  $fpdf = new FPDF();
  $participants = mysqli_query($con, "SELECT * FROM panelists ORDER BY id DESC");
  $id = $_SESSION['event_id'] ?? 1;
  $event = mysqli_query($con, "SELECT * FROM events WHERE id = {$id}");
  $eventdata = mysqli_fetch_assoc($event);
  $font = mysqli_query($con, "SELECT * FROM font WHERE event_id = {$eventdata['id']}");
  $fontdata = mysqli_fetch_assoc($font);
  $fpdf->AddPage();
  $fpdf->AddFont("Custom",'',"{$fontdata['font_name']}.php");
  $count = 1;
  while($row = mysqli_fetch_assoc($participants)){
    $g = strtolower($row['gender']);
    if($eventdata['cert_size'] == 'whole'){
      $fpdf->Image("../../assets/img/{$eventdata['template']}/{$g}-default.jpg",0,0,$fpdf->GetPageWidth(),$fpdf->GetPageHeight());
      $fpdf->SetXY(-45,0);
      $fpdf->SetFont("Arial",'B',12);
      $padnum = str_pad($row['id'],3,'000', STR_PAD_LEFT);
      $fpdf->Cell(0,10,"ccssrnd-thesis12018-{$padnum}",0,0,'C');
      $fpdf->SetXY($fontdata['pos_x'],$fontdata['pos_y']);
      $fpdf->SetFont("Custom","",$fontdata['font_size']);
      $fpdf->Cell(0,10,strtoupper($row['firstname']) ." ".  strtoupper($row['mi']) . ". " . strtoupper($row['lastname']),0,0,'C');
      $fpdf->AddPage();
    }
  }
  $fpdf->Output();
}
else if(isset($_GET['printlist'])){
  $id = $_SESSION['event_id'] ?? 1;
  $event = mysqli_query($con, "SELECT * FROM events WHERE id = {$id}");
  $eventdata = mysqli_fetch_assoc($event);
  $participants = mysqli_query($con, "SELECT * FROM panelists ORDER BY id DESC");
  $fpdf = new FPDF();
  $fpdf->AddPage("L");
  $fpdf->SetFont("Arial","B",11);
  $fpdf->Cell(270,10,"List of participants",0,0,"C");
  $fpdf->Ln();
  $fpdf->Cell(200,10,"Event name: {$eventdata['event_name']}");
  $currentday = date("F d, Y", strtotime($eventdata['date']));
  $fpdf->Cell(100,10, "Event held: {$currentday}");
  $fpdf->Ln();
  $fpdf->Cell(100,10,"Venue: {$eventdata['venue']}");
  $fpdf->Ln();
  $fpdf->Cell(50,10,"Code",1);
  $fpdf->Cell(60,10,"Full name",1);
  $fpdf->Cell(50,10,"Contact number",1);
  $fpdf->Cell(65,10,"Email Address",1);
  $fpdf->Cell(55,10,"Company",1);
  $fpdf->Ln();
  $fpdf->SetFont("Arial","",10);
  while($row = mysqli_fetch_assoc($participants)){
    $fpdf->Cell(50,10,"ccssrnd-thesis12018-{$row['id']}");
    $fpdf->Cell(60,10,"{$row['lastname']}, {$row['firstname']} {$row['mi']}.");
    $fpdf->Cell(50,10,$row['contact']);
    $fpdf->Cell(65,10,$row['email']);
    $fpdf->Cell(55,10,$row['company']);
    $fpdf->Ln();
  }
  $fpdf->Output();
}
else if(isset($_GET['id'])){
  $participant = mysqli_query($con, "SELECT * FROM panelists WHERE id = {$_GET['id']}");
  $fpdf = new FPDF();
  $fpdf->AddPage();
  $row = mysqli_fetch_assoc($participant);
  $fpdf->Image("../../assets/img/certificates/default.jpg",15,0,190,150);
  $fpdf->SetXY(20,75);
  $fpdf->Cell(0,10,strtoupper($row['firstname']) ." ".  strtoupper($row['mi']) . ". " . strtoupper($row['lastname']),0,0,'C');
  $fpdf->SetFont("Arial", "B", 24);
}
else{
  http_response_code(404);
}
?>