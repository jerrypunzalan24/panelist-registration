@include('../header')

<div class ='container'>
<div class ='card' style ='margin-top:100px;width:55%;margin-left:auto;margin-right:auto'>
  <div class ='card-body'>
    @if(Session::get('error')!==null)
      <div class ='alert alert-danger'>
        <div class ='header'><b>Error</b> - {{Session::get('error')}}</div>
      </div>
    @elseif(Session::get('success')!==null)
    <div class ='alert alert-success'>
      <div class ='header'><b>Success</b> - Registered success!</div>
    </div>
    @endif
    <h4>Register</h4>
    <hr/>
    <form method ='post'>
      @csrf
      <div class ='form-group'>
        <input type ='text' name ='username' placeholder ='Username' class ='form-control remove-border' REQUIRED/>
      </div>
      <div class ='form-group'>
          <input type ='password' name ='password' placeholder = 'Password' class ='form-control remove-border' REQUIRED/>
      </div>
      <div class ='form-group'>
        <input type ='submit' name ='submit' style ='width:100%' name ='submit' class ='btn btn-success btn-custom'/>
      </div>
    </form>
  </div>
</div>
</div>