@include("../header")

<div class ='card' style='margin-top:45px;'>
  <div class ='card-body' >
    @if(Session::get("success")!==null)
    <div class ='alert alert-success'>
      <div class ='header'>
        <b>Success</b> - {{Session::get("success")}}
      </div>
    </div>
    @endif
    <h4>Add event</h4>
    <hr/>
    <form method ='POST' action ='{{url()->current()}}/submit'>
      @csrf
      <div class ='row'>
        <div class ='col-md-6'>
          <div class ='form-group'>
            <label>Event name</label>
            <input type ='text' class ='form-control' name ='event_name' placeholder ='Event name' REQUIRED>
          </div>
          <div class ='form-group'>
            <label>Template</label>
            <select name ='template' class ='form-control' REQUIRED>
              @foreach($templates as $template)
              <option value ='{{$template}}'>{{$template}}</option>
              @endforeach
            </select>
          </div>
          <div class ='form-group'>
            <label>Date of the event</label>
            <input type ='datetime-local' class ='form-control' name ='date' REQUIRED>
          </div>
          <div class ='form-group'>
            <label>Venue</label>
            <input type ='text' name ='venue' class ='form-control' placeholder ='Venue' REQUIRED>
          </div>
          <div class ='form-group'>
            <label>Certificate Type</label>
            <select name ='cert_type' class ='form-control' REQUIRED>
              <option value ='landscape'>Landscape</option>
              <option value ='portrait'>Portrait</option>
            </select>
          </div>
          <div class ='form-group'>
            <label>Font <span style ='color:#848484'>(Font not found? Upload your fonts <a href ='#'>here</a>)</span></label>
            <select name ='font' class ='form-control' REQUIRED>
              @foreach($fonts as $font)
              <option value ='{{substr($font, 0, strpos($font, ".php"))}}'>{{ucfirst(substr($font, 0, strpos($font, ".php")))}}</option>
              @endforeach
            </select>
          </div>
          <div class ='row'>
            <div class ='col-md-4'>
              <div class ='form-group'>
                <label>Font size</label>
                <input type ='number' class ='form-control' name ='fontsize' value ='' REQUIRED>
              </div>
            </div>
            <div class ='col-md-4'>
              <div class ='form-group'>
                <label>Font positioning (X Axis)</label>
                <input type ='number' class ='form-control' name ='pos_x' value ='' REQUIRED>
              </div>
            </div>
            <div class ='col-md-4'>
              <div class ='form-group'>
                <label>Font positioning (Y Axis)</label>
                <input type ='number' class ='form-control' name ='pos_y' value ='' REQUIRED>
              </div>
            </div>
          </div>
          <p>Colors</p>
          <hr/>
          <div class ='row'>
            <div class ='col-md-4'>
              <div class ='form-group'>
                <label>Red</label>
                <input type ='number' name ='color_r' placeholder ='Red' class ='form-control' value ='0'>
              </div>
            </div>
            <div class ='col-md-4'>
              <div class ='form-group'>
                <label>Blue</label>
                <input type ='number' name ='color_b' placeholder ='Blue' class ='form-control' value ='0'>
              </div>
            </div>
            <div class ='col-md-4'>
              <div class ='form-group'>
                <label>Green</label>
                <input type ='number' name ='color_g' placeholder ='Green' class ='form-control' value ='0'>
              </div>
            </div>
            <div class ='col-md-12'>
              <div class ='form-group'> 
                <a href ='{{route("preview")}}' target="framename" id ='preview' class ='btn btn-custom btn-success' style ='width:100%'>Preview certificate</a>
              </div>
            </div>
          </div>
        </div>
        <div class ='col-md-6'>
          <p>Required fields </p>
          <hr/>
          <div class ='row form-group'>
            <div class ='col-md-10'>

              <label>Table name</label>
              <input type ='text' name ='table_name' placeholder ='Table name' class ='form-control' value =''>
            </div>
          </div>
          <div class ='row form-group'>
            <div class ='col-md-10'>
              <input type ='text' name ='field[]' class ='form-control' readonly='true' value ='firstname'/>
            </div>
          </div>
          <div class ='row form-group'>
            <div class ='col-md-10'>
              <input type ='text' name ='field[]' class ='form-control' readonly='true' value ='lastname'/>
            </div>
          </div>
          <div class ='row form-group'>
            <div class ='col-md-10'>
              <input type ='text' name ='field[]' class ='form-control' readonly='true' value ='mi'/>
            </div>
          </div>
          <div class ='row form-group'>
            <div class ='col-md-10'>
              <input type ='text' name ='field[]' class ='form-control' readonly='true' value ='gender'/>
            </div>
          </div>
          <p>Optional fields</p>
          <hr/>
          <div id ='field'>   
          </div>
          <button class ='btn btn-success btn-custom' id ='addfield'>Add Field</button>
        </div>
        <div class ='col' style ='margin-top:20px'>
          <input type ='submit' class ='btn btn-success btn-custom' name ='submit' value ='Add Event' style ='width:100%'>
        </div>
      </div>
    </form>
  </div>
</div>
<script src ='{{asset("/assets/js/jquery-3.2.1.min.js")}}'></script>
<script src ='{{asset("/assets/js/bootstrap.min.js")}}'></script>
<script>
    $(document).ready(function(){
     $('#preview').attr('href', `{{route('preview')}}?template=${$('select[name=template] option:selected').val()}&font=${$('select[name=font] option:selected').val()}&cert_type=${$('select[name=cert_type] option:selected').val()}&fontsize=${$('input[name=fontsize').val()}&pos_y=${$('input[name=pos_y]').val()}&pos_x=${$('input[name=pos_x').val()}`)
  })
  $('#addfield').click(function(e){
    e.preventDefault()
    if($('.fields').length < 3){
      $("#field").append(`
        <div class ='fields form-group row'>
        <div class ='col-md-10'>
        <input type ='text' name ='field[]' value ='' class ='form-control' REQUIRED> 
        </div>
        <div class ='col-md-2'>
        <button onclick ="$(this).closest('.fields').detach()" class ='btn btn-success btn-custom'>Close</button>
        </div>
        </div>`)
    }
    else{
      alert("Maximum of 3 only")
    }
  })
    $(`select[name=template], select[name=font], select[name=cert_type], input[name=fontsize], 
    input[name=pos_x], input[name=pos_y], input[name=color_r], input[name=color_g], input[name=color_b]`).change(function(){
$('#preview').attr('href', `{{route('preview')}}?template=${$('select[name=template] option:selected').val()}&font=${$('select[name=font] option:selected').val()}&cert_type=${$('select[name=cert_type] option:selected').val()}&fontsize=${$('input[name=fontsize').val()}&pos_y=${$('input[name=pos_y]').val()}&pos_x=${$('input[name=pos_x').val()}`)
    })
</script>









