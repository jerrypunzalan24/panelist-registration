
@include('header');
<div class = 'container'>
  <div class ='card' style ='margin-top:120px;width:50%;margin-left:auto;margin-right:auto'>
    <div class ='card-body'>

      @if(Session::get("error")!==null)
      <div class ='alert alert-danger'>
        <div class ='header'>
          <b>Error</b> incorrect username or password
        </div>
      </div>
      @elseif(Session::get("success")!==null)
      <div class ='alert alert-success'>
        <div class ='header'>
         {{Session::get("success")}}
        </div>
      </div>
      @endif
      <h4>Login</h4>
      <hr/>
      <form method ='POST' action = '{{url()->current()}}/login'>
        @csrf
        <div class ='form-group'>
          <input type ='text' name ='username' placeholder ='Username' class ='form-control remove-border' REQUIRED>
        </div>
        <div class ='form-group'>
          <input type ='password' name ='password' placeholder ='Password' class ='form-control remove-border' REQUIRED>
        </div>
        <div class ='form-group'>
          <div class ='row'>
            <div class ='col'>
              <input type ='submit' class ='btn btn-success btn-custom' name ='submit'>
            </div>
            <div class ='col' align ='right'>
              <a class ='btn btn-success btn-custom' href ='register/'>Register</a>
            </div>
          </div>
        </div>
      </form>
    </div>
  </div>
</div>