<link rel ='stylesheet' href ='{{asset("/assets/css/bootstrap.min.css")}}'>
<link rel ='stylesheet' href ='{{asset("/assets/font-awesome-4.7.0/css/font-awesome.min.css")}}'>
<style>
   body{
   background-image:url({{asset("/assets/img/bg.png")}});
   background-size:cover;
   background-attachment: fixed;
   }
   input.form-control:focus{
   box-shadow:none;
   }
   input{
   outline:none;
   }
   .remove-border{
   border-top-style:none;border-left-style:none;border-right-style:none;border-radius:0px;
   border-bottom-color: #44ffbb;
   border-bottom-width: 2px;
   }
   @font-face{
   font-family:Custom;
   src:url({{asset("/assets/fonts/SourceSansPro-Regular.ttf")}});
   }
   @font-face{
   font-family:Custom-Bold;
   src:url({{asset("/assets/fonts/Dosis-SemiBold.ttf")}});
   }
   input,p,button,td{
   font-family:Custom;
   }
   h1,h2,h3,h4,h5,b,th{
   font-family:Custom-Bold;
   }
   input.form-control:-webkit-autofill {
   -webkit-box-shadow: 0 0 0 30px #D9FEE5 inset;
   }
   .btn-custom{
   border-radius:0px;box-shadow:1px 2px 3px #666666;
   }
</style>
  <div class ='card' style ='border-radius:0px;position:fixed;top:0;width:100%;z-index:1'>
    <div style ='padding:10px'>
      <div class ='row'>
        <div class ='col'>
      <p onclick ="window.location.href='{{$link}}'"style ='margin-bottom:0px;color:#444444;{{$style}}'>{!!$name!!}</p>
    </div>
    <div class ='col'>
      @if(isset($login))
      <p onclick= "window.location='{{route("logout")}}'" style='text-align:right;margin-bottom:0px;color:#44444;cursor:pointer'>Logout</p>
      @else
      <p style='text-align:right;margin-bottom:0px;color:#44444;'></p>
      @endif

    </div>
    </div>
  </div>
</div>