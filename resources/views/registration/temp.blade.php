
@include('../header');
<div class ='container' >
 <div class ='card' style ='border-radius:0px;border:none;box-shadow:3px 3px 1px #444444;width:65%;margin-left:auto;margin-right:auto;margin-top:50px'>
  <div class ='card-header' style ='background-color:#00C47D;margin-bottom:20px;border-radius:0px' align ='center'>
   <h1 style ='color:white;text-shadow:2px 2px 1px #444444'>REGISTRATION</h1>
   <p style ='color:white;margin-bottom:2px;text-shadow:1px 1px 1px #444444'>Enter all fields below</p>
 </div>
 <div class ='card-body'>
  <form method ='post' id ='submit' >
    @csrf
    <div class ='form-group'>
      @foreach($columns as $row)
      <input type ='hidden' name ='school' value =''/>
      <input type ='hidden' name ='gender' value ='m'/>
      <input type ='hidden' name ='mi' value =''/>
     @if($row->Field != "id" && $row->Field != "event_id" && $row->Field != 'school' && $row->Field != 'gender' && $row->Field != 'mi')
     <div class ='col' style ='padding-left:0px;padding-right:0px;padding-bottom:15px'>
       <input type ='text' name ='{{$row->Field}}'  class ='form-control remove-border' placeholder ='{{ucfirst($row->Field)}}' REQUIRED/>
     </div>
     @endif
     @endforeach
   </div>
   <h5 align ='center'>Disclaimer</h5>
   <hr/>
   <p style ='text-align:justify;color:#545454'>&quot;The event organizers are collecting information from you as participants for the purposes of registration and overall event management.   By providing your information, you are giving your consent to us to use your information for the aforementioned purposes.  
   </p>
   <p style ='text-align: justify;color:#545454'>
     After conclusion of the event and completion of all necessary reports, your personal data will be saved in secure files for future reference and networking activities. If you do not wish to be contacted further after this event, kindly inform the organizers.&quot;
   </p>
   <hr/>
   <div class ='form-check' style ='margin-bottom:30px'>
     <input class ='form-check-input' type ='checkbox' />
     <label class ='form-check-label' style ='color:#545454'>I understand and agree to these terms.</label>
   </div>
   <div class ='form-group'>
     <input type ='submit' name ='submit' class ='btn btn-success btn-block btn-custom' style ='background-color:#00C47D;border-color:#00C47D;margin:auto; width:40% ' disabled ='true' Value ='Done.'/>
   </div>
 </div>
{{-- <div class ='form-group'>
 <input type ='text' name ='company' class ='form-control remove-border' placeholder ='company' REQUIRED/>
</div> --}}

</form>
</div>
</div>
<!-- Prompt modal -->
<div class="modal fade" style ='border-radius:0px' id ='prompt' tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
   <div class="modal-content">
    <div class="modal-header" style ='background-color:#0098BE'>
     <h5 class="modal-title" style ='color:white'>Are you sure?</h5>
     <button type="button" class="close" data-dismiss="modal" aria-label="Close">
       <span aria-hidden="true" style ='color:white'>&times;</span>
     </button>
   </div>
   <div class="modal-body">
     <p>Finalize changes?</p>
   </div>
   <div class="modal-footer">
     <button id ='prompt-yes' type="button" class="btn btn-success btn-custom">Sign me up!</button>
     <button id ='prompt-no' type="button" class="btn btn-primary btn-custom"  data-dismiss="modal">Wait! Let me check.</button>
   </div>
 </div>
</div>
</div>
<!--Success modal -->
<div class="modal fade" style ='border-radius:0px' id ='success' tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
   <div class="modal-content">
    <div class="modal-header" style ='background-color:#0098BE'>
     <h5 class="modal-title" style ='color:white'>Registration Done</h5>
     <button type="button" class="close" data-dismiss="modal" aria-label="Close">
       <span aria-hidden="true" style ='color:white'>&times;</span>
     </button>
   </div>
   <div class="modal-body">
     <p>Welcome to CCSS</p>
   </div>
   <div class="modal-footer">
     <button id ='prompt-yes' data-dismiss ='modal' type="button" class="btn btn-primary btn-custom">Okay</button>
   </div>
 </div>
</div>
</div>
</div>
<script src ='{{asset("/assets/js/jquery-3.2.1.min.js")}}'></script>
<script src ='{{asset("/assets/js/bootstrap.min.js")}}'></script>
<script>
 $('input[type=checkbox]').change(function(){
   $('input[type=submit]').attr('disabled',!$(this).is(":checked"))
 })
 $('#prompt-yes').click(function(){
   $('#prompt').modal('hide')
   $.ajax({
     type: "post",
     url:"{{route("registerparticipant")}}",
     data:$('#submit').serialize(),
     success:function(html){
      console.log(html)
      $('#success').modal('show')
      $('input[type=text], input[type=email], input[type=number]').val("")
      $('input[type=submit]').attr('disabled',true)
      $('input[type=checkbox]').prop('checked',false)
    },
    error:function(html){
      console.log(html)
    }
  })
 })
 $('#submit').submit(function(e){
   e.preventDefault()
   $('#prompt').modal('show')
 })
</script>