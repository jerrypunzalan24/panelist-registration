@include("../header")
<div class ='card' style='margin-top:45px'>
  <div class ='card-body' >
    @if(Session::get("success")!==null)
    <div class ='alert alert-success'>
      <div class ='header'>
        <b>Success</b> - {{Session::get("success")}}
      </div>
    </div>
    @endif
    <h4>Manage events</h4>
    <p>Create, select or edit events</p>
    <hr/>
    <div class ='row' align ='center'>
      @foreach($events as $event)
      <div class ='col-md-4' style ='margin-top:5px'>
        <div class ='card'>
          <div class ='card-body'>
            <img src ='assets/img/{{$event->template}}/m-default.jpg' width ='100%' height ='250px' ><br/>
            <hr/>
            <b>{{$event->event_name}}</b>
            <hr/>
            <form method ='post' action ='{{url()->current()}}/select'>
              @csrf
              <input type ='hidden' name ='event_id' value ='{{$event->id}}'>
              <input type ='hidden' name ='table' value ='{{$event->target_table}}'>
              <input type = 'hidden' name ='type' value ='{{$event->cert_type}}'>
              <input type ='hidden' name ='template' value ='{{$event->template}}'>
              <div class ='form-group'>
                <div class ='row'>
                  <div class ='col-md-6'>
                    <a  href= '{{url()->current()}}/edit/{{$event->id}}' class ='btn btn-success btn-custom' style ='background-color:transparent;color:#333333;box-shadow:none;border-radius:4px;width:100%'>Modify</a>
                  </div>
                  <div class ='col-md-6'>
                    <input type ='submit' name ='submit' value ='Select' class ='btn btn-success btn-custom' style ='background-color:transparent;color:#333333;box-shadow:none;border-radius:4px;width:100%'>
                  </div>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
      @endforeach
      <a href ='{{route("addevents")}}' title ='Add events' style ='position:fixed;bottom:80;right:20;width:75px;height:75px'><img src ='{{asset("assets/img/add-512.png")}}' width ='100%' height ='100%'></a>
    </div>