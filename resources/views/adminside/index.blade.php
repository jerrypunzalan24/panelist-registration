@include('../../header')

<div class ='card' style='margin-top:45px;height:100%;'>
  <div class ='card-body' >
    <h4>Welcome, {{$username}}</h4>
    <hr/>
    <div class ='row' align ='center'>
      <div class ='col-md-4'>
        <div class ='card'>
          <div class ='card-body'>
            <p class ='fa fa-address-book' style ='font-size:15em;background:-webkit-linear-gradient(#9000FF, #01E557);-webkit-background-clip: text;-webkit-text-fill-color: transparent;'></p><br/>
            <a href ='{{url()->current()}}/events'class ='btn btn-primary btn-custom' style ='color:white;width:80%;margin-left:auto;margin-right:auto;background-color:#9000FF;border-color:#9000FF'>
              Select an event
            </a>
          </div>
        </div>
      </div>
      <div class ='col-md-4'>
        <div class ='card'>
          <div class ='card-body'>
            <p class ='fa fa-list-ol' style ='font-size:15em;background:-webkit-linear-gradient(#00c70e, #8EFEB8);-webkit-background-clip: text;-webkit-text-fill-color: transparent;'></p>
            <a href ='{{url()->current()}}/registration/participants'class ='btn btn-primary btn-custom' style ='color:white;width:80%;margin-left:auto;margin-right:auto;background-color:#00C70E;border-color:#00C70E'>
               View participants
            </a>
          </div>
        </div>
      </div>
      <div class ='col-md-4'>
        <div class ='card'>
          <div class ='card-body'>
            <p class ='fa fa-user-plus' style ='font-size:15em;background:-webkit-linear-gradient(#0000ff, #7777ff);-webkit-background-clip: text;-webkit-text-fill-color: transparent;'></p>
            <a href ='{{url()->current()}}/registration'class ='btn btn-primary btn-custom' style ='color:white;width:80%;margin-left:auto;margin-right:auto'>
              Go to registration
            </a>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
